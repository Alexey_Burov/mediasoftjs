;(function () {

    const widthContent = 850;
    const heightContent = 450;

    const liters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K'];

    let container = document.getElementById('content');
    let shots = [];
    let ships = [];

//выравнивание по центру
    container.style = "width:" + widthContent + "px; margin: 0 auto;";

//svg
    let svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    const svgNS = svg.namespaceURI;
    svg.setAttribute('width', widthContent);
    svg.setAttribute('height', heightContent);

    //генерация одной клетки
    function createField(x, y, w, h, id, className, flag) {
        let rect = document.createElementNS(svgNS, 'rect');
        rect.setAttribute('x', x);
        rect.setAttribute('y', y);
        rect.setAttribute('width', w);
        rect.setAttribute('height', h);
        rect.setAttribute('id', id);
        rect.setAttribute('class', className);
        if (flag == 'alien') {
            rect.addEventListener('mousedown', function (evt) {
                if (shots.indexOf(id) == -1) {
                    shots.push(id);
                    console.log('shots:',shots);
                } else {
                    console.warn('repetition');
                }
            }, false);
        } else{
            rect.addEventListener('mousedown', function (evt) {
                if (ships.indexOf(id) == -1) {
                    ships.push(id);
                    console.log('ships:',ships);
                    rect.setAttribute('class', 'ships');
                } else {
                    console.warn('repetition');
                }
            }, false);
        }

        return rect;
    }

    //текст буквы и цифры
    function createText(x, y, text, className) {
        let txt = document.createElementNS(svgNS, 'text');
        txt.setAttribute('x', x);
        txt.setAttribute('y', y);
        txt.textContent = text;
        txt.setAttribute('class', className);
        return txt;
    }

    //контейнер одного поля
    function cretateContainer(x, y, w, h, trnsx, trnsy, flag) {

        let g = document.createElementNS(svgNS, 'g');
        g.setAttribute('transform', 'translate(' + trnsx + ',' + trnsy + ')');

        for (let i = 0; i < 10; i++) {

            for (let j = 0; j < 10; j++) {

                g.appendChild(createField(w / 10 * j, h / 10 * i, w / 10, h / 10, liters[j] + (i + 1), 'field', flag));
            }
            g.appendChild(createText(w / 10 * i + 10, 0 - 10, liters[i], 'text'));
            g.appendChild(createText(-w / 10, h / 10 * (i + 1) - 10, i + 1, 'text'));
        }
        svg.appendChild(g);
    }

    cretateContainer(0, 0, 350, 350, 50, 50, 'own');
    cretateContainer(0, 0, 350, 350, 450, 50, 'alien');
    container.appendChild(svg);


})();