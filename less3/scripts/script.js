///пункт 1
(function () {
    const filtered = films.filter(function (item, i, arr) {
        let ages = item.actors.map(function (actor, j, actors) {
            return actor.age;
        });
        /// средний возраст в одном объекте
        const result = ages.reduce(function (sum, current) {
            return sum + current;
        }, 0);

        /// добавляем объект summ
        item.summ = Math.round(result / ages.length);
        return item.director.oscarsCount == 0;
    });

    let result = {}
    filtered.forEach(function (item, i, arr) {
        if (Object.keys(result).indexOf(item.director.name) < 0) {
            result[item.director.name] = item.summ;
        } else {
            /// если есть повторения режисеров
            result[item.director.name] = Math.round((result[item.director.name] += item.summ) / 2);
        }

    })
    console.log("\nCредний вораст:");
    console.log(result);
})();

///пункт 2
(function () {
    let result = [];
    const filtered = films.filter(function (item, i, arr) {
        const year = item.creationYear;
        item.actors.forEach(function (actor, j, actors) {
            /// если есть оба условия
            if (actor.name == 'Tom Hanks' && year > 1995) {
                /// выбираем всех актеров
                item.actors.forEach(function (a, k, acs) {
                    ///кроме Tom Hanks и повторов
                    if (a.name != 'Tom Hanks' && result.indexOf(a.name) == -1)
                        result.push(a.name);
                })
                return true;
            }
        });
    })
    console.log("\nВсе актеры после 1995:");
    console.log(result);
})();

///пункт 3
(function () {
    const filtered = films.filter(function (item, i, arr) {
        let res = true;

        if (item.director.age > 70) res = false;

        item.actors.forEach(function (actor, j, actors) {
                if (actor.name == 'Tom Hanks') res = false;
            }
        )
        return (res);
    })
    /// после фильтра берем сумму
    let res = 0;
    filtered.forEach(function (item, i, arr) {
        res += item.summ;
    })
    console.log("\nОбщий бюджет:");
    console.log(res);
})();
