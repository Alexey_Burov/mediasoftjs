;(function () {
        let res = prompt("Введите выражение", 'f-dwef3.5f /0.7l-dfs 0.5fsdf +0fs-3sdf*10.0');

        //регулярные выражения
        //цифры и операции
        const rx = new RegExp(/([0-9.*/+-])+/g);
        //только цифры
        const ex = new RegExp(/([*/+-])+/g);
        //только операции
        const num = new RegExp(/([0-9])+/g);
        //убрали лишние символы
        res = res.match(rx);

        if(res.length == 1) res = res.toString().split(ex);

        //массив для чисел и операций
        let term = new Array();
        //заполняем массив term
        for (let i = 0; i < res.length; i++) {
            if (res[i].search(ex) < 0) {
                term.push(res[i]);
            } else {
                let s = res[i].split(ex);
                s.shift();
                if (s[0]) term.push(s[0]);
                if (s[1]) term.push(s[1]);
            }
        }

        //рекурсивная функция чистим массив до первой цифры
        function gotoNum(obj) {
            if (!obj[0].match(num) && obj.length) {
                console.log('выражение должно начинаться с цифры', obj[0]);
                obj.shift();
                gotoNum(obj);
            }
        }

        //
        gotoNum(term);
        //переменная для вычислений
        let calculate = 0;

        //рекурсивная функция вычисляем и сокращаем массив на то что уже вычислено
        function calc(obj) {
            //то что осталось в массиве
            console.log('>>  ', obj);

            if (obj.length < 3) {
                console.warn('STOP длина выражения < 3', obj.length);
                return;
            }
            if (!obj[0].match(num) || !obj[1].match(ex) && !obj[2].match(num)) {
                console.error('не верный формат( num * num =): ', obj[0], obj[1], obj[2]);
                return;
            } else {
                if (obj[1] == '*') {
                    calculate = Number(obj[0]) * Number(obj[2]);
                    console.log('* ', calculate);
                }
                if (obj[1] == '/') {
                    calculate = Number(obj[0]) / Number(obj[2]);
                    console.log('/ ', calculate);
                }
                if (obj[1] == '+') {
                    calculate = Number(obj[0]) + Number(obj[2]);
                    console.log('+ ', calculate);
                }
                if (obj[1] == '-') {
                    calculate = Number(obj[0]) - Number((obj[2]));
                    console.log('- ', calculate);
                }
            }
            obj.shift();
            obj.shift();
            obj[0] = calculate.toString();
            calc(obj);
        }
        calc(term);
        //
        alert('результат вычислений: ' + calculate);
    }
)();
